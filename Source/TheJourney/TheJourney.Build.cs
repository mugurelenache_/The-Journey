using UnrealBuildTool;

public class TheJourney : ModuleRules
{
	public TheJourney(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
	}
}
