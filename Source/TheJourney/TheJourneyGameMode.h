#pragma once
#include "GameFramework/GameMode.h"
#include "TheJourneyGameMode.generated.h"

UCLASS(minimalapi)
class ATheJourneyGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATheJourneyGameMode();
};
