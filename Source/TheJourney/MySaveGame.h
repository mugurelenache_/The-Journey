#pragma once

#include "GameFramework/SaveGame.h"
#include "MySaveGame.generated.h"

UCLASS()
class THEJOURNEY_API UMySaveGame : public USaveGame
{
	GENERATED_BODY()

public:
		UPROPERTY(EditAnywhere, Category = Basic)
		FString PlayerName;

		UPROPERTY(EditAnywhere, Category = Basic)
			FString SaveSlotName;

		UPROPERTY(EditAnywhere, Category = Basic)
			uint32 UserIndex;

		UPROPERTY(EditAnywhere, Category = Basic)
			APawn* PawnActive;

		UMySaveGame();
};
